# eslint-config-midion

This package provides Midion's .eslintrc as an extensible shared eslint config.

## Usage

1. Install eslint and eslint-config-midion:
    ```sh
    npm install eslint eslint-config-midion --save-dev
    ```

2. Create .eslintrc in any of the following formats:

    **.eslintrc.yml**
    ```yml
    extends: midion
    ```

    **.eslintrc.js**
    ```js
    module.exports = { extends: "midion" };
    ```

    **.eslintrc.json**
    ```json
    { "extends": "midion" }
    ```
